module gitee.com/whilew/cream

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/google/uuid v1.3.0
	github.com/imroc/req/v3 v3.11.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	go.etcd.io/etcd/client/v3 v3.5.2
	go.uber.org/multierr v1.8.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	gorm.io/driver/mysql v1.3.2
	gorm.io/driver/sqlserver v1.3.2
	gorm.io/gorm v1.23.5
)
