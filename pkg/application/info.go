package application

import "time"

var (
	startTime = time.Now() // 应用启动时间
)

func GetApplicationStartTime() time.Time {
	return startTime
}
