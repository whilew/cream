package loki

import (
	"gitee.com/whilew/cream/pkg/basic_component/conf"
	"github.com/sirupsen/logrus"
	"sync"
)

func Initialize() error {
	addr := conf.GetString("log.loki.addr")
	if addr != "" {
		return initialize(addr)
	}
	return nil
}

var initonce = sync.Once{}

func initialize(addr string) (err error) {
	initonce.Do(func() {
		default_labels := map[string]string{}
		hook, e := NewPromtailHook(addr, default_labels)
		if e != nil {
			err = e
			return
		}
		logrus.AddHook(hook)
	})

	return err
}
