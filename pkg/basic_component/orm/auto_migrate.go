package orm

import (
	"fmt"
	"gorm.io/gorm"
)

var auto_migrate_models = []interface{}{}

func SetAutoMigrateModels(m ...interface{}) {
	auto_migrate_models = append(auto_migrate_models, m...)
}

func auto_migrate_model(db *gorm.DB) {
	if len(auto_migrate_models) > 0 {
		if err := db.AutoMigrate(auto_migrate_models...); err != nil {
			fmt.Println("auto_migrate_model error:" + err.Error())
		}
	}
}
