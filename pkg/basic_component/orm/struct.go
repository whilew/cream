package orm

import (
	"gorm.io/gorm"
	"time"
)

type BasePageResp struct {
	Count int64       `json:"count"`
	Data  interface{} `json:"data"`
}
type BaseModel struct {
	ID        int            `json:"id" gorm:"primary_key"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	Deleted   gorm.DeletedAt `gorm:"index"`
}
