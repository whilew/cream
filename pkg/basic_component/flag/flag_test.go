package flag

import "testing"

func TestParseFlag(t *testing.T) {
	var test = ""
	Register(&StringFlag{
		Name:     "test",
		Usage:    "测试flag",
		EnvVar:   "test",
		Default:  "123",
		Variable: &test,
	})
	//初始化flag
	if err := Initialize(); err != nil {
		t.Error(err)
	}

}
