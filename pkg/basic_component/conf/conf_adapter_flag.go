package conf

import (
	"os"
	"strings"
)

type ConfAdapterFlag struct {
	Settings
}

func (f *ConfAdapterFlag) Initialize(c *Configuration) error {
	for _, t := range os.Args[1:] {
		kv := strings.Split(t, "=")
		if len(kv) != 2 {
			continue
		}
		f.Set(kv[0], kv[1])
	}
	return nil
}
func (f *ConfAdapterFlag) Set(key string, value interface{}) error {
	f.SetConfig(splitConfigKey(key), value)
	return nil
}
