package conf

type ConfAdapterEtcd struct {
	Endpoints []string `json:"endpoints"`
	Timeout   int      `json:"timeout"`
	Prefix    string   `json:"prefix"`
	Settings
}

func (e *ConfAdapterEtcd) Initialize(c *Configuration) error {
	// todo
	return nil
}
