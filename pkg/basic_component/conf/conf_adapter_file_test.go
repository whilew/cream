package conf

import (
	"fmt"
	"testing"
)

func TestTryFindConfigFilePath(t *testing.T) {
	fmt.Println(tryFindConfigFilePath(&Configuration{}))
}

func TestConfAdapterFile(t *testing.T) {
	f := &ConfAdapterFile{}
	err := f.Initialize(&Configuration{})
	if err != nil {
		t.Error(err)
	}
}
