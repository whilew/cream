package conf

import (
	"os"
	"strings"
)

type ConfAdapterEnv struct {
	Settings
}

func (f *ConfAdapterEnv) Initialize(c *Configuration) error {
	for _, t := range os.Environ() {
		kv := strings.Split(t, "=")
		if len(kv) != 2 {
			continue
		}
		f.Set(kv[0], kv[1])
	}
	return nil
}
func (f *ConfAdapterEnv) Set(key string, value interface{}) error {
	f.SetConfig(splitConfigKey(key), value)
	return nil
}
