// 默认系统变量配置

package conf

// 系统约定得配置key
const (
	/*
		Variable_ApplicationSN 应用名称
		不建议在etcd中配置，推荐使用 SetApplicationName 进行设置
	*/
	Variable_ApplicationSN = "application.sn"
	/*
		Variable_ApplicationEnv 应用环境 dev\debug\release
		不建议在etcd中配置，推荐使用env、flag、file 进行设置
	*/
	Variable_ApplicationEnv = "application.env"

	/*
		Variable_LocalIp 应用本地ip
		推荐使用env、flag 进行设置
	*/
	Variable_LocalIp = "" // todo: 设置本地ip
)

/*
	获取应用名称
	return 应用名称
*/
func GetApplicationName() string {
	return GetStringd(Variable_ApplicationSN, "application")
}

/*
	手动设置应用名称
	para sn=应用名称
*/
func SetApplicationName(sn string) {
	configuration.SetConfig(Variable_ApplicationSN, sn)
}

/*
	获取应用环境
	默认为dev
	return dev\debug\release
*/
func GetApplicationEnv() string {
	return GetStringd(Variable_ApplicationEnv, "dev")
}

/*
	获取应用所在服务器得内网\私有IP
*/
func GetLocalIp() string {
	return GetStringd(Variable_LocalIp, "127.0.0.1")
}

/*
	Deprecated
	获取应用公网ip
	return 公网ip
	todo 解析string,return ip类型
*/
func GetApplicationPubIp() string {
	ip := GetString("application.ip.pub")
	if ip == "" {
		// 如果没有设置，则自动从系统中获取
	}
	return ip
}

/*
	Deprecated
	获取应用内网ip
	return 内网ip
	todo 解析string,return ip类型
*/
func GetApplicationPrivateIp() string {
	ip := GetString("application.ip.private")
	if ip == "" {
		// 如果没有设置，则自动从系统中获取
	}
	return ip
}

/*
	获取应用api host
	*注意 不要以/结尾
	如果有反向代理，应填写实际应用得根路径：
		http://{host}:{port}/{反向代理得路由}
		注意，此处得{反向代理得路由}，指得是在项目中未指定得路由
		例如：http://127.0.0.1:8080/user/login  实际访问地址为 http://www.baidu.com/scrm/user/login
		api host 应填写为：http://www.baidu.com/scrm
		若：http://127.0.0.1:8080/scrm/user/login 实际访问地址为 http://www.baidu.com/scrm/user/login
		则只需填写：http://www.baidu.com 即可
	- 为兼容dm相关配置文件，所以应支持：{application.sn}.host
	- 若{application.sn}.host未获取到值，则使用application.host补齐
	完整etcd配置key：groups.{application.sn}.application.host
	return api host
	grpc、cron类型应用为空
*/
func GetApplicationHost() string {
	host := GetStringd("host", GetString("application.host"))
	if len(host) > 0 && host[len(host)-1] == '/' {
		host = host[:len(host)-1]
	}
	return host
}
