package conf

import (
	"errors"
	"fmt"
	"gitee.com/whilew/cream/pkg/util/xcodec"
	"io/ioutil"
	"path"
	"path/filepath"
)

type ConfAdapterFile struct {
	filePath string
	codec    string
	Settings
	sourceData []byte
}

func (f *ConfAdapterFile) Initialize(c *Configuration) error {
	fmt.Println("-----------------------开始初始化File适配器-----------------------")
	f.filePath = c.GetString("config.file.path")
	if f.filePath == "" {
		fmt.Println("未找到配置项[config.file.path]")
		fmt.Println("开始尝试搜索配置文件")
		if f.filePath = tryFindConfigFilePath(c); f.filePath == "" {
			fmt.Println("未找到配置文件，停止初始化File配置适配器")
			return Err_UnNeedLoadAdapter
		}
		fmt.Println(fmt.Sprintf("自动搜索到配置文件[%s]", f.filePath))
	}
	return f.loadConfigFile()
}

// tryFindConfigFilePath 尝试寻找配置文件
func tryFindConfigFilePath(c *Configuration) string {
	filePath := c.GetStringd("config.file.name", "config.yaml")
	for i := 0; i < 10; i++ {
		// 检查配置文件是否存在
		if _, err := ioutil.ReadFile(filePath); err == nil {
			return filePath
		}
		filePath = "../" + filePath
	}
	return ""
}

func (f *ConfAdapterFile) Set(key string, value interface{}) error {
	return errors.New("not support")
}

func (f *ConfAdapterFile) loadConfigFile() error {
	absolutePath, err := filepath.Abs(f.filePath)
	if err != nil {
		return errors.New(fmt.Sprintf("get file config absolute path error. path:%s, err: %v", f.filePath, err))
	}
	fmt.Println(fmt.Sprintf("开始加载配置文件[%s]", absolutePath))
	contentData, err := ioutil.ReadFile(absolutePath)
	if err != nil {
		return errors.New(fmt.Sprintf("read file by absolute path error. absolute_path:%s, err: %v", absolutePath, err))
	}
	f.sourceData = contentData
	// todo 判断是否需要输出配置文件内容
	switch path.Ext(f.filePath) {
	case ".yaml":
		f.codec = xcodec.XcodecYaml
	case ".json":
		f.codec = xcodec.XcodecJson
	default:
		f.codec = xcodec.XcodecJson
	}
	conf := map[string]interface{}{}
	if err := xcodec.Decode(f.codec, contentData, &conf); err != nil {
		return errors.New(fmt.Sprintf("decode file config data failed: %v，codec: %s", err, f.codec))
	}
	f.Settings.SetConfigByMap(conf)
	fmt.Println("File配置适配器初始化完成")
	return nil
}
