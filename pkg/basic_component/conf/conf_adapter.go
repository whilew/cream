package conf

import "errors"

type IAdapter interface {
	IAction
	Initialize(c *Configuration) error
	Set(key string, value interface{}) error
}

var (
	// Err_UnNeedLoadAdapter 当未满足该适配器加载条件时返回异常
	Err_UnNeedLoadAdapter = errors.New("不需要加载该配置文件")
)
