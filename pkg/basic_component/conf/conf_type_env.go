package conf

import (
	"os"
	"strings"
)

func get_env_config(key string) string {
	for _, t := range os.Environ() {
		kv := strings.Split(t, "=")
		if len(kv) >= 2 && strings.ToLower(strings.Join(splitConfigKey(kv[0]), ".")) == key {
			configuration.SetConfig(key, kv[1])
			return kv[1]
		}
	}
	return ""
}
