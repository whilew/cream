package conf

type IAction interface {
	Print(key string)
	Get(key string) interface{}
	GetString(key string) string
	GetStringd(key, dv string) string
	GetBool(key string) bool
	GetBoold(key string, dv bool) bool
	GetInt(key string) int
	GetIntd(key string, dv int) int
	GetInt64(key string) int64
	GetInt64d(key string, dv int64) int64
	GetFloat64(key string) float64
	GetFloat64d(key string, dv float64) float64
	GetStringSlice(key string) []string
	GetChildd(key string) *Settings
}
