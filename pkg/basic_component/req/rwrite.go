package req

import (
	"context"
	"github.com/imroc/req/v3"
	"io"
	"net/http"
	"net/url"
	"time"
)

func GetReqCli() *req.Request {
	return req.R()
}

// SetFormDataFromValues is a global wrapper methods which delegated
// to the default client, create a request and SetFormDataFromValues for request.
func SetFormDataFromValues(data url.Values) *req.Request {
	return req.SetFormDataFromValues(data)
}

// SetFormData is a global wrapper methods which delegated
// to the default client, create a request and SetFormData for request.
func SetFormData(data map[string]string) *req.Request {
	return req.SetFormData(data)
}

// SetCookies is a global wrapper methods which delegated
// to the default client, create a request and SetCookies for request.
func SetCookies(cookies ...*http.Cookie) *req.Request {
	return req.SetCookies(cookies...)
}

// SetQueryString is a global wrapper methods which delegated
// to the default client, create a request and SetQueryString for request.
func SetQueryString(query string) *req.Request {
	return req.SetQueryString(query)
}

// SetFileReader is a global wrapper methods which delegated
// to the default client, create a request and SetFileReader for request.
func SetFileReader(paramName, filePath string, reader io.Reader) *req.Request {
	return req.SetFileReader(paramName, filePath, reader)
}

// SetFileBytes is a global wrapper methods which delegated
// to the default client, create a request and SetFileBytes for request.
func SetFileBytes(paramName, filename string, content []byte) *req.Request {
	return req.SetFileBytes(paramName, filename, content)
}

// SetFiles is a global wrapper methods which delegated
// to the default client, create a request and SetFiles for request.
func SetFiles(files map[string]string) *req.Request {
	return req.SetFiles(files)
}

// SetFile is a global wrapper methods which delegated
// to the default client, create a request and SetFile for request.
func SetFile(paramName, filePath string) *req.Request {
	return req.SetFile(paramName, filePath)
}

// SetFileUpload is a global wrapper methods which delegated
// to the default client, create a request and SetFileUpload for request.
func SetFileUpload(f ...req.FileUpload) *req.Request {
	return req.SetFileUpload(f...)
}

// SetResult is a global wrapper methods which delegated
// to the default client, create a request and SetResult for request.
func SetResult(result interface{}) *req.Request {
	return req.SetResult(result)
}

// SetError is a global wrapper methods which delegated
// to the default client, create a request and SetError for request.
func SetError(error interface{}) *req.Request {
	return req.SetError(error)
}

// SetBearerAuthToken is a global wrapper methods which delegated
// to the default client, create a request and SetBearerAuthToken for request.
func SetBearerAuthToken(token string) *req.Request {
	return req.SetBearerAuthToken(token)
}

// SetBasicAuth is a global wrapper methods which delegated
// to the default client, create a request and SetBasicAuth for request.
func SetBasicAuth(username, password string) *req.Request {
	return req.SetBasicAuth(username, password)
}

// SetHeaders is a global wrapper methods which delegated
// to the default client, create a request and SetHeaders for request.
func SetHeaders(hdrs map[string]string) *req.Request {
	return req.SetHeaders(hdrs)
}

// SetHeader is a global wrapper methods which delegated
// to the default client, create a request and SetHeader for request.
func SetHeader(key, value string) *req.Request {
	return req.SetHeader(key, value)
}

// SetOutputFile is a global wrapper methods which delegated
// to the default client, create a request and SetOutputFile for request.
func SetOutputFile(file string) *req.Request {
	return req.SetOutputFile(file)
}

// SetOutput is a global wrapper methods which delegated
// to the default client, create a request and SetOutput for request.
func SetOutput(output io.Writer) *req.Request {
	return req.SetOutput(output)
}

// SetQueryParams is a global wrapper methods which delegated
// to the default client, create a request and SetQueryParams for request.
func SetQueryParams(params map[string]string) *req.Request {
	return req.SetQueryParams(params)
}

// SetQueryParam is a global wrapper methods which delegated
// to the default client, create a request and SetQueryParam for request.
func SetQueryParam(key, value string) *req.Request {
	return req.SetQueryParam(key, value)
}

// AddQueryParam is a global wrapper methods which delegated
// to the default client, create a request and AddQueryParam for request.
func AddQueryParam(key, value string) *req.Request {
	return req.AddQueryParam(key, value)
}

// SetPathParams is a global wrapper methods which delegated
// to the default client, create a request and SetPathParams for request.
func SetPathParams(params map[string]string) *req.Request {
	return req.SetPathParams(params)
}

// SetPathParam is a global wrapper methods which delegated
// to the default client, create a request and SetPathParam for request.
func SetPathParam(key, value string) *req.Request {
	return req.SetPathParam(key, value)
}

// MustGet is a global wrapper methods which delegated
// to the default client, create a request and MustGet for request.
func MustGet(url string) *req.Response {
	return req.MustGet(url)
}

// Get is a global wrapper methods which delegated
// to the default client, create a request and Get for request.
func Get(url string) (*req.Response, error) {
	return req.Get(url)
}

// MustPost is a global wrapper methods which delegated
// to the default client, create a request and Get for request.
func MustPost(url string) *req.Response {
	return req.MustPost(url)
}

// Post is a global wrapper methods which delegated
// to the default client, create a request and Post for request.
func Post(url string) (*req.Response, error) {
	return req.Post(url)
}

// MustPut is a global wrapper methods which delegated
// to the default client, create a request and MustPut for request.
func MustPut(url string) *req.Response {
	return req.MustPut(url)
}

// Put is a global wrapper methods which delegated
// to the default client, create a request and Put for request.
func Put(url string) (*req.Response, error) {
	return req.Put(url)
}

// MustPatch is a global wrapper methods which delegated
// to the default client, create a request and MustPatch for request.
func MustPatch(url string) *req.Response {
	return req.MustPatch(url)
}

// Patch is a global wrapper methods which delegated
// to the default client, create a request and Patch for request.
func Patch(url string) (*req.Response, error) {
	return req.Patch(url)
}

// MustDelete is a global wrapper methods which delegated
// to the default client, create a request and MustDelete for request.
func MustDelete(url string) *req.Response {
	return req.MustDelete(url)
}

// Delete is a global wrapper methods which delegated
// to the default client, create a request and Delete for request.
func Delete(url string) (*req.Response, error) {
	return req.Delete(url)
}

// MustOptions is a global wrapper methods which delegated
// to the default client, create a request and MustOptions for request.
func MustOptions(url string) *req.Response {
	return req.MustOptions(url)
}

// Options is a global wrapper methods which delegated
// to the default client, create a request and Options for request.
func Options(url string) (*req.Response, error) {
	return req.Options(url)
}

// MustHead is a global wrapper methods which delegated
// to the default client, create a request and MustHead for request.
func MustHead(url string) *req.Response {
	return req.MustHead(url)
}

// Head is a global wrapper methods which delegated
// to the default client, create a request and Head for request.
func Head(url string) (*req.Response, error) {
	return req.Head(url)
}

// SetBody is a global wrapper methods which delegated
// to the default client, create a request and SetBody for request.
func SetBody(body interface{}) *req.Request {
	return req.SetBody(body)
}

// SetBodyBytes is a global wrapper methods which delegated
// to the default client, create a request and SetBodyBytes for request.
func SetBodyBytes(body []byte) *req.Request {
	return req.SetBodyBytes(body)
}

// SetBodyString is a global wrapper methods which delegated
// to the default client, create a request and SetBodyString for request.
func SetBodyString(body string) *req.Request {
	return req.SetBodyString(body)
}

// SetBodyJsonString is a global wrapper methods which delegated
// to the default client, create a request and SetBodyJsonString for request.
func SetBodyJsonString(body string) *req.Request {
	return req.SetBodyJsonString(body)
}

// SetBodyJsonBytes is a global wrapper methods which delegated
// to the default client, create a request and SetBodyJsonBytes for request.
func SetBodyJsonBytes(body []byte) *req.Request {
	return req.SetBodyJsonBytes(body)
}

// SetBodyJsonMarshal is a global wrapper methods which delegated
// to the default client, create a request and SetBodyJsonMarshal for request.
func SetBodyJsonMarshal(v interface{}) *req.Request {
	return req.SetBodyJsonMarshal(v)
}

// SetBodyXmlString is a global wrapper methods which delegated
// to the default client, create a request and SetBodyXmlString for request.
func SetBodyXmlString(body string) *req.Request {
	return req.SetBodyXmlString(body)
}

// SetBodyXmlBytes is a global wrapper methods which delegated
// to the default client, create a request and SetBodyXmlBytes for request.
func SetBodyXmlBytes(body []byte) *req.Request {
	return req.SetBodyXmlBytes(body)
}

// SetBodyXmlMarshal is a global wrapper methods which delegated
// to the default client, create a request and SetBodyXmlMarshal for request.
func SetBodyXmlMarshal(v interface{}) *req.Request {
	return req.SetBodyXmlMarshal(v)
}

// SetContentType is a global wrapper methods which delegated
// to the default client, create a request and SetContentType for request.
func SetContentType(contentType string) *req.Request {
	return req.SetContentType(contentType)
}

// SetContext is a global wrapper methods which delegated
// to the default client, create a request and SetContext for request.
func SetContext(ctx context.Context) *req.Request {
	return req.SetContext(ctx)
}

// DisableTrace is a global wrapper methods which delegated
// to the default client, create a request and DisableTrace for request.
func DisableTrace() *req.Request {
	return req.DisableTrace()
}

// EnableTrace is a global wrapper methods which delegated
// to the default client, create a request and EnableTrace for request.
func EnableTrace() *req.Request {
	return req.EnableTrace()
}

// EnableDumpTo is a global wrapper methods which delegated
// to the default client, create a request and EnableDumpTo for request.
func EnableDumpTo(output io.Writer) *req.Request {
	return req.EnableDumpTo(output)
}

// EnableDumpToFile is a global wrapper methods which delegated
// to the default client, create a request and EnableDumpToFile for request.
func EnableDumpToFile(filename string) *req.Request {
	return req.EnableDumpToFile(filename)
}

// SetDumpOptions is a global wrapper methods which delegated
// to the default client, create a request and SetDumpOptions for request.
func SetDumpOptions(opt *req.DumpOptions) *req.Request {
	return req.SetDumpOptions(opt)
}

// EnableDump is a global wrapper methods which delegated
// to the default client, create a request and EnableDump for request.
func EnableDump() *req.Request {
	return req.EnableDump()
}

// EnableDumpWithoutBody is a global wrapper methods which delegated
// to the default client, create a request and EnableDumpWithoutBody for request.
func EnableDumpWithoutBody() *req.Request {
	return req.EnableDumpWithoutBody()
}

// EnableDumpWithoutHeader is a global wrapper methods which delegated
// to the default client, create a request and EnableDumpWithoutHeader for request.
func EnableDumpWithoutHeader() *req.Request {
	return req.EnableDumpWithoutHeader()
}

// EnableDumpWithoutResponse is a global wrapper methods which delegated
// to the default client, create a request and EnableDumpWithoutResponse for request.
func EnableDumpWithoutResponse() *req.Request {
	return req.EnableDumpWithoutResponse()
}

// EnableDumpWithoutRequest is a global wrapper methods which delegated
// to the default client, create a request and EnableDumpWithoutRequest for request.
func EnableDumpWithoutRequest() *req.Request {
	return req.EnableDumpWithoutRequest()
}

// EnableDumpWithoutRequestBody is a global wrapper methods which delegated
// to the default client, create a request and EnableDumpWithoutRequestBody for request.
func EnableDumpWithoutRequestBody() *req.Request {
	return req.EnableDumpWithoutRequestBody()
}

// EnableDumpWithoutResponseBody is a global wrapper methods which delegated
// to the default client, create a request and EnableDumpWithoutResponseBody for request.
func EnableDumpWithoutResponseBody() *req.Request {
	return req.EnableDumpWithoutResponseBody()
}

// SetRetryCount is a global wrapper methods which delegated
// to the default client, create a request and SetRetryCount for request.
func SetRetryCount(count int) *req.Request {
	return req.SetRetryCount(count)
}

// SetRetryInterval is a global wrapper methods which delegated
// to the default client, create a request and SetRetryInterval for request.
func SetRetryInterval(getRetryIntervalFunc req.GetRetryIntervalFunc) *req.Request {
	return req.SetRetryInterval(getRetryIntervalFunc)
}

// SetRetryFixedInterval is a global wrapper methods which delegated
// to the default client, create a request and SetRetryFixedInterval for request.
func SetRetryFixedInterval(interval time.Duration) *req.Request {
	return req.SetRetryFixedInterval(interval)
}

// SetRetryBackoffInterval is a global wrapper methods which delegated
// to the default client, create a request and SetRetryBackoffInterval for request.
func SetRetryBackoffInterval(min, max time.Duration) *req.Request {
	return req.SetRetryBackoffInterval(min, max)
}

// SetRetryHook is a global wrapper methods which delegated
// to the default client, create a request and SetRetryHook for request.
func SetRetryHook(hook req.RetryHookFunc) *req.Request {
	return req.SetRetryHook(hook)
}

// AddRetryHook is a global wrapper methods which delegated
// to the default client, create a request and AddRetryHook for request.
func AddRetryHook(hook req.RetryHookFunc) *req.Request {
	return req.AddRetryHook(hook)
}

// SetRetryCondition is a global wrapper methods which delegated
// to the default client, create a request and SetRetryCondition for request.
func SetRetryCondition(condition req.RetryConditionFunc) *req.Request {
	return req.SetRetryCondition(condition)
}

// AddRetryCondition is a global wrapper methods which delegated
// to the default client, create a request and AddRetryCondition for request.
func AddRetryCondition(condition req.RetryConditionFunc) *req.Request {
	return req.AddRetryCondition(condition)
}

// SetUploadCallback is a global wrapper methods which delegated
// to the default client, create a request and SetUploadCallback for request.
func SetUploadCallback(callback req.UploadCallback) *req.Request {
	return req.SetUploadCallback(callback)
}

// SetUploadCallbackWithInterval is a global wrapper methods which delegated
// to the default client, create a request and SetUploadCallbackWithInterval for request.
func SetUploadCallbackWithInterval(callback req.UploadCallback, minInterval time.Duration) *req.Request {
	return req.SetUploadCallbackWithInterval(callback, minInterval)
}

// SetDownloadCallback is a global wrapper methods which delegated
// to the default client, create a request and SetDownloadCallback for request.
func SetDownloadCallback(callback req.DownloadCallback) *req.Request {
	return req.SetDownloadCallback(callback)
}

// SetDownloadCallbackWithInterval is a global wrapper methods which delegated
// to the default client, create a request and SetDownloadCallbackWithInterval for request.
func SetDownloadCallbackWithInterval(callback req.DownloadCallback, minInterval time.Duration) *req.Request {
	return req.SetDownloadCallbackWithInterval(callback, minInterval)
}
