package xerror

import (
	"errors"
	"fmt"
	"go.uber.org/multierr"
)

type Dberror struct {
	Err error
}

func (e *Dberror) Error() string {
	return e.Err.Error()
}
func (e *Dberror) Append(msg string) *Dberror {
	e.Err = Append(e.Err, msg)
	return e
}
func NewDberror(err error) *Dberror {
	return &Dberror{Err: err}
}

func Append(err error, msg string) error {
	return multierr.Append(err, errors.New(msg))
}

type BusinessError struct {
	Code  int    `json:"code"`
	Codes string `json:"codes"`
	Msg   string `json:"msg"`
}

func NewBusinessError(code int, msg string) *BusinessError {
	return &BusinessError{
		Code: code,
		Msg:  msg,
	}
}
func NewBusinessErrors(codes string, msg string) *BusinessError {
	return &BusinessError{
		Codes: codes,
		Msg:   msg,
	}
}
func (e *BusinessError) Error() string {
	return fmt.Sprintf("code[%s] msg[%s]", e.Codes, e.Msg)
}
