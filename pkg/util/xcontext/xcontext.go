package xcontext

import (
	"context"
	"unsafe"
)

type valueCtx struct {
	context.Context
	key, val interface{}
}

type iface struct {
	itab, data uintptr
}

// todo 有异常，重新设计
func GetValueCtxKV(ctx context.Context) map[interface{}]interface{} {
	m := make(map[interface{}]interface{})
	return m
	getValueCtxKV(ctx, m)
	return m
}

func getValueCtxKV(ctx context.Context, m map[interface{}]interface{}) {
	ictx := *(*iface)(unsafe.Pointer(&ctx))
	if ictx.data == 0 {
		return
	}

	valCtx := (*valueCtx)(unsafe.Pointer(ictx.data))
	if valCtx.Context != nil && valCtx != nil && valCtx.key != nil && valCtx.val != nil {
		m[valCtx.key] = valCtx.val
	}
	getValueCtxKV(valCtx.Context, m)
}
