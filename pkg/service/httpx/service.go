package httpx

import (
	"gitee.com/whilew/cream/pkg/basic_component/conf"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

/*
	日志集成
*/

var (
	DefaultHttpListernPort = 8080
)

type Service struct {
	*gin.Engine
}

func NewService() *Service {
	return &Service{
		Engine: newGin(),
	}
}

func (s *Service) StartUp() error {
	http_listern_port := conf.GetIntd("http.port", DefaultHttpListernPort)
	server := &http.Server{
		Addr:    ":" + strconv.Itoa(http_listern_port),
		Handler: s.Engine,
	}
	return server.ListenAndServe()
}
func (s *Service) Stop() error {
	return nil
}
