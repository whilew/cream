package httpx

import "github.com/gin-gonic/gin"

func newGin() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	//开启gin
	r := gin.New()
	// 注入请求日志
	r.Use(Middleware_SetReqId())
	// 跨域
	r.Use(Middleware_Cors())
	// 记录请求日志
	r.Use(Middleware_ReqLog)
	//捕获异常
	r.Use(Middleware_Recover)
	return r
}
