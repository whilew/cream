package httpx

import (
	"fmt"
	"gitee.com/whilew/cream/pkg/basic_component/log"
	"github.com/gin-gonic/gin"
	"time"
)

const (
	context_key_req_log_loger = "req_log_loger"
)

func Middleware_ReqLog(c *gin.Context) {
	start := time.Now()
	loger := GetLoger(c)
	c.Set(context_key_req_log_loger, loger)
	c.Next()
	end := time.Now()
	// GET /user/info 200 200ms
	loger.Info(fmt.Sprintf("%s %s %d %s", c.Request.Method, c.Request.RequestURI, c.Writer.Status(), fmt.Sprint(end.Sub(start))))
}
func SetReqLogLable(c *gin.Context, k, v string) {
	loger := c.MustGet(context_key_req_log_loger).(*log.Loger)
	loger.WithKV(k, v)
}
