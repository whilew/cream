## Conf
### File
config.file.path=配置文件地址
### Etcd
etcd.addr=配置etcd地址,多个使用逗号隔开  
etcd.username=配置etcd用户名称  
etcd.password=配置etcd密码  
etcd.prefix=配置etcd中需要读取的配置key的前缀  
etcd.cli=etcd客户端实例
### System Variable
application.sn=应用的名称  
application.env=应用当前环境  
application.ip.pub=应用公网ip  
application.ip.private=应用内网ip  
application.host=应用对外的域名（如果非80端口需要带上端口）  

## Log
### Loki
log.loki.addr=loki的地址  

## Orm
orm.dsn=数据库链接字符串,格式：mysql:xxxxx  
orm.conns.open.max=orm最大打开连接数  
orm.conns.idle.max=orm最大活跃连接  

## Service
http.port=http监听端口  