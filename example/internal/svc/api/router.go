package api

import (
	"gitee.com/whilew/cream/example/internal/svc/handler"
	"gitee.com/whilew/cream/pkg/service/httpx"
)

func InitApiRouter(s *httpx.Service) {
	r := s.Group("user")
	{
		r.GET("getconf", handler.GetUserInfo)
		r.GET("dberror", handler.GetDBError)
		r.GET("testlog", handler.GetTestLog)
	}
}
