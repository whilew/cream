package handler

import (
	"gitee.com/whilew/cream/example/model"
	"gitee.com/whilew/cream/pkg/basic_component/conf"
	"gitee.com/whilew/cream/pkg/service/httpx"
	"gitee.com/whilew/cream/pkg/util/xerror"
	"github.com/gin-gonic/gin"
)

func GetUserInfo(c *gin.Context) {
	/*
			获取配置
			对应的yaml文件为：
			user:
		 	  name: 小明
	*/
	userName := conf.GetStringd("user.name", "小明")
	httpx.OkWithData(c, userName)
}

func GetDBError(c *gin.Context) {
	db := httpx.GetOrm(c) // 从上下文中获取orm实例 - 自动将reqId 注入到orm中，用于orm日志的定位[待优化]
	u := &model.User{}
	if err := db.First(u).Error; err != nil { // todo 可重写db.First()方法，自动返回Db类型的异常，省略xerror.NewDberror()的调用
		httpx.FailWithError(c, xerror.NewDberror(err)) // 正式环境将自动隐藏真实db异常内容，输入[数据库异常，请稍后重试],并将真正的异常记录到日志
		return
	}
	httpx.OkWithData(c, u)
}
func GetTestLog(c *gin.Context) {
	loger := httpx.GetLoger(c)
	loger.Info("test log") // 自动将reqId 注入到loger中，可根据reqId获取异常、系统日志
	httpx.Ok(c)
}
