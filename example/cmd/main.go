package main

import (
	"gitee.com/whilew/cream/example/internal/svc/api"
	"gitee.com/whilew/cream/example/model"
	"gitee.com/whilew/cream/pkg/application"
	"gitee.com/whilew/cream/pkg/basic_component/conf"
	"gitee.com/whilew/cream/pkg/basic_component/orm"
	"gitee.com/whilew/cream/pkg/service/httpx"
)

func main() {
	conf.SetApplicationName("example")           // 设置app的名称(可以用于日志的搜索、etcd自动读取配置[待重写]、服务注册发现[暂未实现]...)
	autoMigrateModel()                           // 自动迁移模型
	application.Initialize()                     // 初始化config\orm\logger等基础组件
	application.StartUpService(newHttpService()) // 初始化服务型组件
	application.KeepAlive()                      // app keep alive
}

// newHttpService 初始化http服务
func newHttpService() *httpx.Service {
	httpx.DefaultHttpListernPort = 8080 // 定义默认的http端口
	s := httpx.NewService()
	api.InitApiRouter(s) // 初始化api路由
	return s
}

// autoMigrateModel 自动迁移模型
func autoMigrateModel() {
	orm.SetAutoMigrateModels(&model.User{})
}
