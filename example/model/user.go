package model

type User struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func (User) TableName() string {
	return "user"
}
