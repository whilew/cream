## conf 模块加载逻辑

## 使用到的环境变量\flag
- config.codec 默认配置的解码方式：json\yaml (如果根据配置存储类型无法反推出codec则使用此配置，默认为yaml)
- config.file.path 文件配置地址：config.yaml (根据该配置解析指定位置的配置文件，如果为空并且没有可以使用的配置存储类型，则默认读取config.yaml配置文件)

## 配置存储类型
### file
配置文件
支持类型： yaml、json  
不使用【config.codec】配置