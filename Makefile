# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build

all:build
build:
	$(GOBUILD) -o example/bin/example.exe example/main.go
run:
	$(GOBUILD) -o example/bin/example.exe example/main.go
	./example/bin/example.exe